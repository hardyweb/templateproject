@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
<h1>Profile</h1>
@stop

@section('content')

<!-- Main content -->
    <section class="content">
 
        Name :{{ $profile->name }} <br/>
        NRIC : {{ $profile->nric }} <br/>
        Address : {{ $profile->address }} <br/>
        Poscode : {{ $profile->poscode }} <br/>
        Dob : {{ $profile->dob->format('d/m/Y') }} <br/>
        Age : {{ $profile->age }}<br/>
        State: {{ $profile->state->name }} <br/>
        Position : {{ $profile->position->name }} <br/>


    </section>
    <!-- /.content -->
  
  <!-- /.content-wrapper -->



@stop
 
 