## Sistem Pangkalan Data Pekerja

Sistem ini merupakan sebuah sistem yang digunakan sebagai contoh dalam proses pembelajaran dan pengajaran Laravel asas 

+ Menggunakan Laravel versi 8 
+ Template AdminLTE 3

## Pre build 

1. User 
2. State
3. Position 


## Install 

1. git clone https://gitlab.com/hardyweb/templateproject.git pekerja

2. composer install 

3. cp .env.example .env

4. setup database pekerja

5. php artisan migrate

6. php artisan optimize

7. php artisan route:cache

8. php aritisan route:clear

9. php artisan key:generate

10. php artisan ui bootstrap --auth

11. php artisan db:seed UserSeeder


#user 


user: admin@gmail.com
pass : 12345678

user : kerani@gmail.com
pass : 12345678

