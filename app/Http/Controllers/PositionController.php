<?php

namespace App\Http\Controllers;

use App\Models\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$positions = Position::all();

       // $position = Position::orderBy('susun','desc')->get();

        $positions = Position::orderBy('susun')->get();


        return view('position.index', compact('positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('position.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $position = new Position(); 
        try {
          DB::beginTransaction();
         
          $position->name = $request->input('name');
          $position->susun = $request->input('susun');





          



  
          if($position->save()){
          DB::commit();
            return redirect('/position')->with('successMessage', 'Maklumat position Berjaya Kemaskini');
      
  
          }else{
          DB::rollBack();
          return back()->with('errorMessage', 'Maklumat state tidak berjaya  Kemaskini');
  
          }
          } catch (\Throwable $e ){
          DB::rollBack();
                      return back()->withInput()->with('errorMessage', 'Maklumat tidak berjaya  Kemaskini'.$e->getMessage());
  
  
  
        }
  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function show(Position $position)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function edit($position)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $position)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function destroy($position)
    {
        $position = Position::findorFail($position);
        if($position->delete()){
            return redirect('/position')->with('successMessage','position Telah Dipadamkan');
        }

    }
}
