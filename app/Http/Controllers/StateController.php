<?php

namespace App\Http\Controllers;

use App\Http\Requests\StateRequest;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = State::all();
        return view('state.index', compact('states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('state.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StateRequest $request)
    {
        $state = new State(); 
        try {
          DB::beginTransaction();
         
          $state->name = $request->input('name');
         
  
                if($state->save()){
          DB::commit();
            return redirect('/state')->with('successMessage', 'Maklumat state Berjaya Kemaskini');
      
  
          }else{
          DB::rollBack();
          return back()->with('errorMessage', 'Maklumat state tidak berjaya  Kemaskini');
  
          }
          } catch (\Throwable $e ){
          DB::rollBack();
                      return back()->withInput()->with('errorMessage', 'Maklumat tidak berjaya  Kemaskini'.$e->getMessage());
  
  
  
        }
  




    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit($state)
    {
   
        $states = State::findOrFail($state);
 

	    return view('state.edit',compact('states'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(StateRequest $request, $state)
    {
    
        $state = State::findOrFail($state);

        try {
            DB::beginTransaction();
           
            $state->name = $request->input('name');
           
    
                  if($state->save()){
            DB::commit();
              return redirect('/state')->with('successMessage', 'Maklumat state Berjaya Kemaskini');
        
    
            }else{
            DB::rollBack();
            return back()->with('errorMessage', 'Maklumat state tidak berjaya Kemaskini');
    
            }
            } catch (\Throwable $e ){
            DB::rollBack();
                        return back()->withInput()->with('errorMessage', 'Maklumat tidak berjaya Kemaskini'.$e->getMessage());
    
    
    
          }







    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(State $state)
    {
        //
    }
}
