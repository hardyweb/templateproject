<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Models\Position;
use App\Models\Profile;
use App\Models\State;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = Profile::all();
        return view ('profile.index', compact('profiles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $states = State::all();
       $positions= Position::orderBy('susun')->get();
        
        return view('profile.create',compact('states','positions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileRequest $request)
    {
        $profile = new Profile(); 
        try {
          DB::beginTransaction();
         
          $profile->name = $request->input('name');
          $profile->nric = $request->input('nric');
          $profile->address = $request->input('address');
          $profile->poscode= $request->input('poscode');
          $profile->dob =  Carbon::createFromFormat('d/m/Y',$request->input('dob'));
          $profile->state_id = $request->input('state');
          $profile->position_id = $request->input('position');
         
  
         if($profile->save()){
          DB::commit();
            return redirect('/profile')->with('successMessage', 'Maklumat state Berjaya Kemaskini');
      
  
          }else{
          DB::rollBack();
          return back()->with('errorMessage', 'Maklumat state tidak berjaya  Kemaskini');
  
          }
          } catch (\Throwable $e ){
          DB::rollBack();
                      return back()->withInput()->with('errorMessage', 'Maklumat tidak berjaya  Kemaskini'.$e->getMessage());
  
  
  
        }
  



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show($profile)
    {
        $profile = Profile::findOrFail($profile);

        return view('profile.show', compact('profile'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
