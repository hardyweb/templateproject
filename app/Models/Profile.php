<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $dates =['dob'];


    public function getAgeAttribute()
	{
		return Carbon::parse($this->attributes['dob'])->age;
	}

    public function state(){

		return $this->hasOne('App\Models\State','id','state_id');

	}

    public function position(){

		return $this->hasOne('App\Models\Position','id','position_id');

	}

    public function profile(){

		return $this->belongsTo('App\Models\Profile');

	}




}
