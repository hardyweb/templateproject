<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

               Gate::define('admin', function ($user) {
                 if ($user->role == 'admin'){
                                 return $user->role== 'admin';
     		                  }
                   return false;
               });

               Gate::define('kerani', function ($user) {
                if ($user->role == 'kerani'){
                                   return $user->role== 'kerani';
                                 }
                     return false;
                 });




    }
}
