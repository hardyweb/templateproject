<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');

    //return view('auth.login');
});

Auth::routes();
 
Route::group(['middleware' => ['auth']], function () {

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    

    //State 
   
    Route::get('/state',[App\Http\Controllers\StateController::class,'index']);
    Route::get('/state/create',[App\Http\Controllers\StateController::class,'create']);
    Route::post('/state/create',[App\Http\Controllers\StateController::class,'store']);
    Route::get('/state/{id}/view',[App\Http\Controllers\StateController::class,'view']);
    Route::get('/state/{id}/edit',[App\Http\Controllers\StateController::class,'edit']);
    Route::post('/state/{id}/edit',[App\Http\Controllers\StateController::class,'update']);
    Route::get('/state/{id}/delete',[App\Http\Controllers\StateController::class,'destroy']);
    
    //position
    Route::get('/position',[App\Http\Controllers\PositionController::class,'index']);
    Route::get('/position/create',[App\Http\Controllers\PositionController::class,'create']);
    Route::post('/position/create',[App\Http\Controllers\PositionController::class,'store']);
    Route::get('/position/{id}/view',[App\Http\Controllers\PositionController::class,'view']);
    Route::get('/position/{id}/edit',[App\Http\Controllers\PositionController::class,'edit']);
    Route::post('/position/{id}/edit',[App\Http\Controllers\PositionController::class,'update']);
    Route::get('/position/{id}/delete',[App\Http\Controllers\PositionController::class,'destroy']);
    
      //profile
    Route::get('/profile',[App\Http\Controllers\ProfileController::class,'index']);
    Route::get('/profile/create',[App\Http\Controllers\ProfileController::class,'create']);
    Route::post('/profile/create',[App\Http\Controllers\ProfileController::class,'store']);
    Route::get('/profile/{id}/show',[App\Http\Controllers\ProfileController::class,'show']);
    Route::get('/profile/{id}/edit',[App\Http\Controllers\ProfileController::class,'edit']);
    Route::post('/profile/{id}/edit',[App\Http\Controllers\ProfileController::class,'update']);
    Route::get('/profile/{id}/delete',[App\Http\Controllers\ProfileController::class,'destroy']);
      
  


    Route::resource('users',UsersController::class);
    
});
    

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
